require_relative "human_factory"

puts HumanFactory.create.about_me
puts HumanFactory.create(:lawyer).about_me
puts HumanFactory.create(:politician).about_me
