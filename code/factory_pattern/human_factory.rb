require_relative "human"
require_relative "politician"
require_relative "lawyer"

class HumanFactory
  def self.create human_type=nil
    case human_type
    when :lawyer
      Lawyer.new
    when :politician
      Politician.new
    else
      Human.new
    end
  end
end
