module Observer
  attr_accessor :observers
  
  def initialize
    @observers = []
  end

  def notify_observers
    @observers.each do |observer|
      observer.notify self
    end
  end
end
