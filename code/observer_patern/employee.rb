require_relative "observer"

class Employee
  attr_reader :salary
  include Observer

  def change_salary new_salary
    @salary = new_salary
    notify_observers
  end
end
