require_relative "employee"
require_relative "pay_roll"
require_relative "tax_department"

employee = Employee.new
employee.observers =  [PayRoll.new, TaxDepartment.new]
employee.change_salary 5000
