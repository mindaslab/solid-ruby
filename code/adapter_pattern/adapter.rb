require_relative "dog_module"
require_relative "cat_module"

module Adapter
  include DogModule
  include CatModule
end
